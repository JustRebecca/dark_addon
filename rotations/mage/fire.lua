-- Firemage for 8.1 by Rotations - 8/2018
-- Talents: 2 2 1 3 2 2 1
-- Holding Alt = DragonsBreath
-- Holding Shift = Flamestrike

local addon, dark_addon = ...
local SB = dark_addon.rotation.spellbooks.mage

local function combat()

  if target.alive and target.enemy and player.alive then

local inRange = 0
    for i = 1, 40 do
      if UnitExists('nameplate' .. i) and IsSpellInRange('Ebonbolt', 'nameplate' .. i) == 1 and UnitAffectingCombat('nameplate' .. i) then 
        inRange = inRange + 1
      end 
end
   dark_addon.interface.status_extra('R:' .. dark_addon.version .. '   T#:' .. inRange .. ' |cff5BFF33 D:|r ' .. target.distance)
   
     -- added
    if castable(SB.Counterspell, 'target') and target.interrupt(100, false) then
      return cast(SB.Counterspell, 'target')
    end

	 if toggle('polymorph', false) and not -target.debuff(SB.Polymorph) then
		return cast(SB.Polymorph, 'target')
		end

	if modifier.alt and -spell(SB.DragonsBreath) == 0 then
      return cast(SB.DragonsBreath, 'target')
    end
    if modifier.shift and -spell(SB.Flamestrike) == 0 and -buff(SB.HotStreak) then
      return cast(SB.Flamestrike, 'ground')
    end

    if -buff(SB.HotStreak) then
      return cast(SB.Pyroblast, 'target')
    end
    if -buff(SB.HeatingUp) and spell(SB.FireBlast).charges > 0 and toggle('phoenix_flames', false) then
      return cast(SB.FireBlast)
	  end
    if -buff(SB.HeatingUp) and spell(SB.PhoenixFlames).charges > 0 and toggle('phoenix_flames', false) then
      return cast(SB.PhoenixFlames)
	  end

    if player.moving  then
      return cast(SB.Scorch, 'target')
    end
    if -spell(SB.BlazingBarrier) == 0 and not -buff(SB.BlazingBarrier) and toggle('blazing_barrier', false) then
      return cast(SB.BlazingBarrier, 'player')
    end
    if -spell(SB.Combustion) == 0 and toggle('cooldowns', false) then
      return cast(SB.Combustion, 'player')
    end

    if -buff(SB.Combustion) then
	    return cast(SB.Scorch)
	   end


    return cast(SB.Fireball, 'target')
    end
end

local function resting()

  local inRange = 0
    for i = 1, 40 do
      if UnitExists('nameplate' .. i) and IsSpellInRange('Ebonbolt', 'nameplate' .. i) == 1 and UnitAffectingCombat('nameplate' .. i) then 
        inRange = inRange + 1
      end 
end
   dark_addon.interface.status_extra('R:' .. dark_addon.version .. '   T#:' .. inRange .. ' |cff5BFF33 D:|r ' .. target.distance)

	if toggle('blazing_barrier', false) and -spell(SB.BlazingBarrier) == 0 and not -buff(SB.BlazingBarrier) and player.moving then
    return cast(SB.BlazingBarrier, 'player')
  end

  if player.spell(SB.Shimmer).lastcast and ugh==nil then
     PlaySoundFile("Interface\\AddOns\\WeakAuras\\PowerAurasMedia\\Sounds\\sonar.ogg", "Master")
     ugh = 1
     return cast(SB.ArcaneInt, player)
   end

   if ugh == 1 then
    ugh = nil
  end

  -- Put great stuff here to do when your out of combat
end

local function interface()
   dark_addon.interface.buttons.add_toggle({
    name = 'blazing_barrier',
    label = 'Blazing Barrier',
    font = 'dark_addon_icon',
    on = {
      label = dark_addon.interface.icon('globe'),
      color = dark_addon.interface.color.blue,
      color2 = dark_addon.interface.color.ratio(dark_addon.interface.color.dark_blue, 0.7)
    },
    off = {
      label = dark_addon.interface.icon('globe'),
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })

  dark_addon.interface.buttons.add_toggle({
    name = 'phoenix_flames',
    on = {
      label = 'PF',
      color = dark_addon.interface.color.red,
      color2 = dark_addon.interface.color.ratio(dark_addon.interface.color.dark_orange, 0.7)
    },
    off = {
      label = 'PF',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })

  dark_addon.interface.buttons.add_toggle({
    name = 'polymorph',
    on = {
      label = 'PM',
      color = dark_addon.interface.color.green,
      color2 = dark_addon.interface.color.ratio(dark_addon.interface.color.dark_orange, 0.7)
    },
    off = {
      label = 'PM',
      color = dark_addon.interface.color.grey,
      color2 = dark_addon.interface.color.dark_grey
    }
  })
end

dark_addon.rotation.register({
  spec = dark_addon.rotation.classes.mage.fire,
  name = 'fire',
  label = 'Bundled Fire',
  combat = combat,
  resting = resting,
  interface = interface
})

--{ "Pyroblast", { "player.buff(Kael'thas's Ultimate Ability)", "!player.buff(48108)", "!modifier.last(Pyroblast)", "!player.moving"}},
-- "player.spell(Fire Blast).charges < 1"
